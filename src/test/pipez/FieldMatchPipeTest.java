package pipez;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

import org.junit.Test;

import pipez.core.Block;
import pipez.core.SimpleBlock;
import static pipez.util.TestUtils.*;

public class FieldMatchPipeTest {

	@Test
	public void test_match() {
		SimpleBlock sb = new SimpleBlock("abc", "cde", "efg", "ghi" );
		System.out.println(sb.value("C3"));
		
		FieldMatchPipe select = FieldMatchPipe.create("C3");
		Block b = select.transform(sb);
		//System.out.println(b.value("C1"));

		assertThat(b.values().length, is(4));
		assertThat(b.values(), are("efg"));
		
	}
	
	@Test
	public void test_nomatch() {
		SimpleBlock sb = new SimpleBlock("abc", "cde", "efg", "ghi" );
		
		FieldMatchPipe select = FieldMatchPipe.create("C5");
		Block b = select.transform(sb);

		assertThat(b.values().length, is(0));
	}
	
	@Test public void test_nomatch_case() {
		
		SimpleBlock sb = new SimpleBlock("abc", "cde", "efg", "ghi" );
		
		FieldMatchPipe select = FieldMatchPipe.create("c3");
		Block b = select.transform(sb);

		assertThat(b.values().length, is(0));
	}
	
	@Test
	public void test_nomatch_inverse() {
		SimpleBlock sb = new SimpleBlock("abc", "cde", "efg", "ghi");
		//System.out.println(sb.value("C3"));
		FieldMatchPipe select = FieldMatchPipe.createInverse("something that won't match");
		
		Block b = select.transform(sb);
		
		//System.out.println(sb.value("C3"));
		
		
		assertThat(b.values().length, is(4));
		assertThat(b.values(), are("abc", "cde", "efg", "ghi" ));
	}

	@Test
	public void test_match_inverse() {
		SimpleBlock sb = new SimpleBlock("abc", "cde", "efg", "ghi" );
		
		FieldMatchPipe select = FieldMatchPipe.createInverse(sb.value("C3"));
		Block b = select.transform(sb);

		assertThat(b.values().length, is(0));
	}

	@Test
	public void test_match_ignorecase() {
		SimpleBlock sb = new SimpleBlock("ABC", "CDE", "DEF", "FGH" );
		
		FieldMatchPipe select = FieldMatchPipe.createInverse(sb.value("C3"));
		Block b = select.transform(sb);

		assertThat(b.values().length, is(0));
	}
	
	@Test
	public void test_nomatch_ignorecase() {
		fail();
	}
	
}
