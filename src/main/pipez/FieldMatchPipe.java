package pipez;

import pipez.core.Block;
import pipez.core.Pipe;

import static pipez.core.SpecialBlocks.*;

/**
 * Returns only Blocks which have at least one field name which has the given string.
 * e.g. for a given CSV file, only lines which contain the given string are passed through.
 * 
 * Also includes an inverse-mode, which does not pass through Blocks that have at least
 * one field name which has the given string.
 * 
 * @author whwong
 *
 */
public class FieldMatchPipe implements Pipe {

	@Override
	public String getName() {
		return "Field Match";
	}

	private String toMatch = null;
	private boolean inverse = false, ignoreCase = false;
	
	private FieldMatchPipe(String toMatch, boolean inverse, boolean ignoreCase) {
		this.toMatch = toMatch;
		this.inverse = inverse;
		this.ignoreCase = ignoreCase;
		
	}
	
	public static FieldMatchPipe create(String toMatch) {
		return new FieldMatchPipe(toMatch, false, false);
	}
	
	public static FieldMatchPipe createIgnoreCase(String toMatch) {
		return new FieldMatchPipe(toMatch, false, true);
	}

	public static FieldMatchPipe createInverse(String toMatch) {
		return new FieldMatchPipe(toMatch, true, false);
	}
	
	public static FieldMatchPipe createInverseIgnoreCase(String toMatch) {
		return new FieldMatchPipe(toMatch, true, true);
	}
	
	@Override
	public Block transform(Block block) {
		boolean flagReturnsBlockIfTrue = true;
		boolean flag1 = true;
		
		for(String v :block.values()) {
			if (!flag1) {
				flagReturnsBlockIfTrue = false;
			}
			if (inverse) {
				if(v.equals((toMatch))) {
					flag1 = false;
				}		
				
			} else {		
				if(!v.equals(toMatch)) {
					flag1 = false;
				}
			}
			
		
		}
		if(flagReturnsBlockIfTrue) {
			return block;
		} else {
			return SKIP_BLOCK;
		}
		
		
		
		
		/*
		boolean flag = false;
		for(String v :block.values()) {
			System.out.println(v);
			if(v.equals(toMatch) & !inverse) {
				flag = true;
			} else if (v.equals(toMatch) & inverse) {
				flag = false;
				break;
			} else if (!v.equals(toMatch) & !inverse) {
				flag = true;
			}
		}
		if (flag) {
			return block;
		} else {
			return SKIP_BLOCK;
		}
		*/

	}

}
